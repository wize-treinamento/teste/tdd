class Calculadora:

    def somar (self, valor_1, valor_2):
        return valor_1 + valor_2
    
    def subtrair(self, valor_1, valor_2):
        return valor_1 - valor_2
    
    def multiplicar (self, valor_1, valor_2):
        return valor_1 * valor_2
    
    def dividir (self, valor_1, valor_2):
        
        if valor_2 == 0:
            raise ZeroDivisionError()

        return valor_1 / valor_2
        