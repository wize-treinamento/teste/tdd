import unittest
from calculadora import Calculadora

class TestCalculadora(unittest.TestCase):

    def setUp(self):
        self.calculadora = Calculadora()

    def test_somar(self):
        
        resultado = self.calculadora.somar(1,1)
        resultado_experado = 2
        self.assertEqual(resultado, resultado_experado)
    
    def test_subtrair(self):
        
        resultado = self.calculadora.subtrair(1,1)
        resultado_experado = 0
        self.assertEqual(resultado, resultado_experado)

if __name__ == '__main__':
    unittest.main()